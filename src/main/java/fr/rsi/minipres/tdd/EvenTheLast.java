package fr.rsi.minipres.tdd;

/*On vous donne une liste d'entiers. Vous devez trouver la somme des éléments dont les indices sont pairs (0ème, 2ème, 4ème...) 
et ensuite multiplier ce total par le dernier élément de la liste. N'oubliez pas que l'indice du premier élément est 0.

Pour une liste vide, le résultat sera toujours 0 (zéro).

Entrée : Une liste (list) d'entiers (int).

Sortie : Le résultat en tant qu'entier (int).

Précondition : 0 ≤ len(liste) ≤ 20
all(isinstance(x, int) for x in liste)
all(-100 < x < 100 for x in liste)*/

public class EvenTheLast {

}
