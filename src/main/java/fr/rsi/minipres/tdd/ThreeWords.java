package fr.rsi.minipres.tdd;

/*On va apprendre aux Robots à distinguer les mots et les nombres.

On vous donne une chaine de caractères avec des mots et des nombres séparés par des caractères blancs (un espace). Les mots contiennent uniquement des lettres. Vous devez vérifier si la chaine de caractères contient trois mots d'affilée. Par exemple, la chaine de caractères "start 5 one two three 7 end" contient trois mots d'affilée.

Entrée : Une chaine de caractères avec des mots.

Sortie : La réponse en tant que booléen.

Précondition : L'entrée contient des mots et/ou des nombres. Il n'y a pas de mot mixte (lettres et chiffres mélangés).
0 < len(mots) < 100*/

public class ThreeWords {

}
